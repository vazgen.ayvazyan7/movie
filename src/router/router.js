import MainPage from "@/pages/MainPage";
import MoviesPage from "../pages/MoviesPage";
import SingleMovieInfo from "@/pages/SingleMovieInfo";
import ProjectPage from "../pages/ProjectPage";
import {createRouter,createWebHistory} from "vue-router";


const routes = [

    {
        path:'/',
        component: MainPage
    },
    {
        path:'/movies',
        component: MoviesPage,
        props:true
    },

    {
        path:'/movies/:id',
        component: SingleMovieInfo
    },
    {
        path:'/project',
        component: ProjectPage,
    },

];

const router = createRouter({
    routes,
    history:createWebHistory(process.env.BASE_URL)

});



export default router
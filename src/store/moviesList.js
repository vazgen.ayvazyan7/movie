import axios from "axios";

export const moviesList = {
    state: {
        movies: [],
        singleMovie: '',
        page: 1,
        limit: 4,
        totalPages: 0,
        search:''
    },

    mutations: {
        SET_MOVIES: (state, movies) => {
            state.movies = movies
        },
        SET_PAGE: (state, page) => {
            state.page = page
        },
        SET_LIMIT: (state, limit) => {
            state.limit = limit
        },
        SET_TOTAL_PAGE: (state, totalPages) => {
            state.totalPages = totalPages
        },
        SET_SINGLE_MOVIE: (state, singleMovie) => {
            state.singleMovie = singleMovie

        },
        SET_SEARCH:(state,search)=> {
            state.search =search
        }
    },

    actions: {
        async GET_MOVIES_FROM_API({state, commit}) {

            try {
                const response = await axios.get('https://yts.mx/api/v2/list_movies.json', {

                    params: {
                        _page: state.page,
                        _limit: state.limit
                    }

                });
                commit('SET_MOVIES', response.data.data.movies);
                commit('SET_TOTAL_PAGE', Math.ceil(response.data.data.limit / this.state));

            } catch (e) {
                alert('Ошибка');

            }
        },

    },
    getters: {
        MOVIES(state) {
            return state.movies;

        },
        TOTAL_PAGE(state) {
            return state.totalPages;

        },
        PAGE(state) {
            return state.page;
        },
        LIMIT(state) {
            return state.limit;
        },
        SET_SEARCH() {
            return state.movies.filter(movie => {
                return movie.title.toLowerCase().includes(state.search.toLowerCase())
            })
        },


    },
};

export default moviesList



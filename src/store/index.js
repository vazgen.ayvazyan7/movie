import {createStore} from 'vuex'

import {moviesList} from "./moviesList";

// import {moviesList} from '@/store/moviesList'


export default createStore({
    modules: {
        movies:moviesList
    },

})